neonate
=======

Neonate is a very basic suite of network applications coded in Python.  It is designed to be simple, efficient and easy to understand, enabling the novice programmer to modify it to suit his or her needs.
